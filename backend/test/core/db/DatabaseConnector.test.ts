import "reflect-metadata";

import {Logger} from "graublau";
import {DatabaseConnector} from "../../../src/core/db/DatabaseConnector";
import {MongoConnector} from "../../../src/core/db/MongoConnector";

describe("DatabaseConnector", () => {
    let mongoConnector: Partial<MongoConnector>;
    let logger: Partial<Logger>;
    let connector: DatabaseConnector;

    beforeEach(() => {
        logger = {
            error: jest.fn(),
            info: jest.fn(),
        };
        mongoConnector = {};

    });

    it("succeeds at first attempt",  async () => {
        mongoConnector.connect = jest.fn().mockResolvedValueOnce("");

        connector = new DatabaseConnector(mongoConnector as MongoConnector, 1, logger as Logger);

        await connector.connect();

        expect(logger.info as jest.Mock).toBeCalled();
    });

    it("succeeds after several failed attempts",  async (done) => {
        const error: Error = new Error("unable to connect");
        mongoConnector.connect = jest.fn()
            .mockRejectedValueOnce(error)
            .mockRejectedValueOnce(error)
            .mockResolvedValue(true);

        connector = new DatabaseConnector(mongoConnector as MongoConnector, 1, logger as Logger);

        const promise = connector.connect();

        setTimeout(async () => {
            await promise;
            expect((mongoConnector.connect as jest.Mock).mock.calls.length).toBe(3);
            expect((logger.error as jest.Mock).mock.calls.length).toBe(2);
            expect(logger.info as jest.Mock).toBeCalled();

            done();
        }, 100);
    });

    it("fails after max attempts",  async (done) => {
        const error: Error = new Error("unable to connect");
        mongoConnector.connect = jest.fn().mockRejectedValue(error);

        connector = new DatabaseConnector(mongoConnector as MongoConnector, 1, logger as Logger);

        const promise = connector.connect();

        setTimeout(async () => {
            await promise;
            expect((mongoConnector.connect as jest.Mock).mock.calls.length).toBe(DatabaseConnector.MAX_ATTEMPTS);
            expect((logger.error as jest.Mock).mock.calls.length).toBe(DatabaseConnector.MAX_ATTEMPTS + 1);

            done();
        }, 100);
    });
});
