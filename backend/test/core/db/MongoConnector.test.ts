import {Mongoose} from "mongoose";
import "reflect-metadata";
import {MongoConnector} from "../../../src/core/db/MongoConnector";

describe("MongoConnector", () => {
    const connectUri = "foo";
    let mongoose: Partial<Mongoose>;
    let connector: MongoConnector;

    beforeEach(() => {
        mongoose = {
            connect: jest.fn(),
            set: jest.fn(),
        };
        connector = new MongoConnector(mongoose as any, connectUri);
    });

    it("connects via mongoose", () => {
        connector.connect().then(() => {
            expect(mongoose.connect).toBeCalledWith(connectUri, {useNewUrlParser: true, useCreateIndex: true});
        }).catch((err) => {
            expect(err).toBe(null);
        });
    });

    it("returns information as string", () => {
        const expected: string = "Mongo foo";
        const result: string = connector.toString();

        expect(result).toBe(expected);
    });
});
