import "reflect-metadata";
// tslint:disable-next-line
import {Request, Response} from "express";
import {validationResult} from "express-validator";
import {Logger} from "graublau";
import {TestController} from "./__fixtures/TestController";

jest.genMockFromModule("express-validator");
jest.mock("express-validator");

describe("Controller", () => {
    const ERROR_MESSAGE = "I'm out of tea!";

    let statusResponse: Partial<Response>;
    let response: Partial<Response>;
    let request: Partial<Request>;
    let logger: Partial<Logger>;

    let controller: TestController;

    beforeEach(() => {
        statusResponse = {};
        response = {};
        request = {};
        logger = {
            error: jest.fn(),
        };

        statusResponse.json = jest.fn().mockReturnValue({});
        statusResponse.send = jest.fn().mockReturnValue({});
        response.status = jest.fn().mockReturnValue(statusResponse as Response);
    });

    it("handles success", async () => {
        setUpValidation(true);

        controller = new TestController(logger as Logger);
        controller.validationError(request as Request, response as Response);

        await flushPromises();
        expectResponse(200, {});
    });

    it("handles validation error", async () => {
        setUpValidation(false);

        controller = new TestController(logger as Logger);
        controller.validationError(request as Request, response as Response);

        await flushPromises();
        expectResponse(400, {status: ERROR_MESSAGE});
    });

    it("handles application error", async () => {
        setUpValidation(false);

        controller = new TestController(logger as Logger);
        controller.applicationError(request as Request, response as Response);

        await flushPromises();
        expectResponse(500, {error: "something happened"});
    });

    const setUpValidation = (success: boolean) => {
        const errors: any[] = [
            {msg: ERROR_MESSAGE},
        ];

        (validationResult as any).mockImplementation(() => {
            return {
                array: jest.fn().mockReturnValue(errors),
                isEmpty: jest.fn().mockReturnValue(success),
            };
        });
    };

    const expectResponse = (code: number, body: any) => {
        expect(response.status as jest.Mock).toHaveBeenCalledWith(code);
        expect(statusResponse.json as jest.Mock).toHaveBeenCalledWith(body);
    };

    const flushPromises = () => {
        return new Promise((resolve) => setImmediate(resolve));
    };
});
