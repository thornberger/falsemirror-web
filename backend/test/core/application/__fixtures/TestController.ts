import {Request, Response} from "express";
import {validationResult} from "express-validator";
import {Logger} from "graublau";
import {provide} from "inversify-binding-decorators";
import {Controller} from "../../../../src/core/application/Controller";

@provide(TestController)
export class TestController extends Controller {
    constructor(logger: Logger) {
        super(logger);

        this.validationError = this.validationError.bind(this);
        this.applicationError = this.applicationError.bind(this);
    }

    public validationError(request: Request, response: Response): void {
        const errors = validationResult(request);
        if (!errors.isEmpty()) {
            return this.handleValidationError(errors, response);
        }

        response.status(200).json({});
    }

    // @ts-ignore
    public applicationError(request: Request, response: Response): void {
        return this.handleApplicationError(new Error("something happened"), response);
    }
}
