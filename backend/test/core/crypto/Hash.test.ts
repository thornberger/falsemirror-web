import "reflect-metadata";
import {Hash} from "../../../src/core/crypto/Hash";

describe("Hash", () => {
    let hash: Hash;

    beforeAll(() => {
        hash = new Hash();
    });

    it("creates a valid sha256 hash", () => {
        const message: string = "hello world";
        const secret: string = "this is secret";
        const expected: string = "7e0309151f1face91727aa7a834a155a433cce00d5981a800566b9919db8a529";

        const result = hash.hmac(message, secret);

        expect(result).toBe(expected);
    });
});
