import "reflect-metadata";
import * as requestPromise from "request-promise-native";
import {CaptchaConfigInterface} from "../../../src/core/captcha/CaptchaConfigInterface";
import {Verifier} from "../../../src/core/captcha/Verifier";

jest.genMockFromModule("request-promise-native");
jest.mock("request-promise-native");

describe("Verifier", () => {
    const config: CaptchaConfigInterface = {
        secret: "1234",
    };
    let verifier: Verifier;

    beforeEach(() => {
        verifier = new Verifier(config);
    });

    it("isVerified successfully", async () => {
        (requestPromise as any).mockImplementation(() => {
            return Promise.resolve({success: true});
        });

        const result = await verifier.isVerified("foo", "127.0.0.1");
        expect(result).toBeTruthy();
    });

    it("isVerified unsuccessfully", async () => {
        (requestPromise as any).mockImplementation(() => {
            return Promise.resolve({success: false});
        });

        const result = await verifier.isVerified("foo", "127.0.0.1");
        expect(result).toBeFalsy();
    });
});
