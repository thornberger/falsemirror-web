import "reflect-metadata";
import {Mail} from "../../../src/core/mail/Mail";
import {MailOptionsInterface} from "../../../src/core/mail/MailOptionsInterface";

describe("Mail", () => {
    const sender = "foo@bar.com";
    const recipients = ["a@b.com"];
    const template = "test";
    const variables = {
        foo: "bar",
    };

    class TestMail extends Mail {
        protected getTemplate(): string {
            return template;
        }

        protected async getVariables(): Promise<object> {
            return variables;
        }
    }

    let mail: Mail;

    beforeEach(() => {
        mail = new TestMail();
    });

    it("returns data with template and variables", async () => {
        const expected: MailOptionsInterface = {
            locals: variables,
            message: {
                from: sender,
                to: recipients,
            },
            template,
        };

        const result: MailOptionsInterface = await mail.getData(sender, recipients);

        expect(result).toEqual(expected);
    });

});
