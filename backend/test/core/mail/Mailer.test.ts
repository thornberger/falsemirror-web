import "reflect-metadata";

import * as EmailTemplate from "email-templates";
import {Mail} from "../../../src/core/mail/Mail";
import {Mailer} from "../../../src/core/mail/Mailer";

describe("Mailer", () => {
    const sender = "foo@bar.com";
    const recipients = ["a@b.com"];

    let mail: Partial<Mail>;
    let emailTemplate: Partial<EmailTemplate>;
    let mailer: Mailer;

    beforeEach(() => {
        emailTemplate = {
            send: jest.fn(),
        };
        mail = {};
        mailer = new Mailer(emailTemplate as EmailTemplate);
    });

    it("sends data from mail", async () => {
        const data = {foo: "bar"};
        mail.getData = jest.fn().mockResolvedValue(data);
        await mailer.send(mail as Mail, sender, recipients);

        expect(emailTemplate.send).toBeCalledWith(data);
    });

});
