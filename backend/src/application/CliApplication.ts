import {Script} from "../core/application/Script";
import {TokenImportScript} from "../resources/token/TokenImportScript";
import {BuildFrontendScript} from "../resources/webhook/hooks/buildFrontend/BuildFrontendScript";
import {Application as BaseApplication} from "./Application";
import {TYPES} from "./Types";

export class CliApplication extends BaseApplication {

    private scripts: { [name: string]: Script } = {};

    protected execute(): void {
        const logger = this.getLogger();
        const argv = this.getArgv();

        const name = this.getScriptNameFromArguments(argv);
        const script = this.getScriptByName(name);

        script.run().then(() => {
            process.exit(0);
        }).catch((error) => {
            logger.error(error);
            process.exit(1);
        });
    }

    protected async initialize(): Promise<void> {
        this.registerScripts();
        return super.initialize();
    }

    private registerScripts(): void {
        this.scripts.TokenImport = this.module.getContainer().get(TokenImportScript);
        this.scripts.BuildFrontend = this.module.getContainer().get(BuildFrontendScript);
    }

    private getScriptNameFromArguments(argv: string[]): string {
        const name = argv[2];
        if (!name) {
            const message = "No script name provided.";
            throw new Error(message);
        }

        return name;
    }

    private getScriptByName(name: string): Script {
        if (!this.scripts.hasOwnProperty(name)) {
            const message: string = `Could not find script ${name}. ` +
                "Make sure the script exists and was registered in the CLI application.";
            throw new Error(message);
        }

        return this.scripts[name];
    }

    private getArgv(): string[] {
        return this.module.getContainer().get(TYPES.ProcessArgv);
    }

}
