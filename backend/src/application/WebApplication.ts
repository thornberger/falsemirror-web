import {Application as BaseApplication} from "graublau";
import {TokenResource} from "../resources/token/TokenResource";
import {WebhookResource} from "../resources/webhook/WebhookResource";
import {Application} from "./Application";

export class WebApplication extends Application {

    protected execute(): void {
        const application: BaseApplication = this.getBaseApplication();

        application.addResource("api/token", this.module.getContainer().get(TokenResource));
        application.addResource("api/webhook", this.module.getContainer().get(WebhookResource));
        application.start();
    }

    private getBaseApplication(): BaseApplication {
        return this.module.getContainer().get(BaseApplication);
    }
}
