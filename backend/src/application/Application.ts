import {Logger} from "graublau";
import {DatabaseConnector} from "../core/db/DatabaseConnector";
import {Module} from "./Module";

export abstract class Application {

    protected module: Module;

    // noinspection TypeScriptAbstractClassConstructorCanBeMadeProtected
    public constructor(module: Module) {
        this.module = module;

        this.execute = this.execute.bind(this);
        this.initialize = this.initialize.bind(this);
        this.getLogger = this.getLogger.bind(this);
        this.getDatabaseConnector = this.getDatabaseConnector.bind(this);
    }

    public run(): void {
        this.initialize().then(this.execute).catch(this.getLogger().error);
    }

    protected abstract execute(): void;

    protected initialize(): Promise<void> {
        const connector: DatabaseConnector = this.getDatabaseConnector();

        return connector.connect();
    }

    protected getLogger(): Logger {
        return this.module.getContainer().get(Logger);
    }

    protected getDatabaseConnector(): DatabaseConnector {
        return this.module.getContainer().get(DatabaseConnector);
    }
}
