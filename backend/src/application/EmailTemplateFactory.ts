import EmailTemplate from "email-templates";
import {Logger} from "graublau";
import {inject} from "inversify";
import {provide} from "inversify-binding-decorators";
import nodemailer from "nodemailer";
import Mail from "nodemailer/lib/mailer/index";
import * as SMTPTransport from "nodemailer/lib/smtp-transport";
import {TYPES} from "./Types";

@provide(EmailTemplateFactory)
export class EmailTemplateFactory {
    private readonly options: SMTPTransport.Options;
    private logger: Logger;

    public constructor(@inject(TYPES.SMTPTransportOptions) options: SMTPTransport.Options, logger: Logger) {
        this.options = options;
        this.logger = logger;
    }

    public create(): EmailTemplate {
        const mail: Mail = nodemailer.createTransport(this.options);
        mail.verify((error: Error | null) => {
            if (error !== null) {
                this.logger.error("Unable to connect to mail service.", error);
            } else {
                this.logger.info("Successfully connected to mail service");
            }
        });

        return new EmailTemplate(
            {
                message: {},
                send: true,
                transport: mail,
                views: {
                    options: {
                        extension: "ejs",
                    },
                    root: __dirname + "../../../templates/email/",
                },
            });
    }
}
