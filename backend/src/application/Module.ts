import * as commander from "commander";
import EmailTemplate = require("email-templates");
import {Config, Module as BaseModule} from "graublau";
import {Container} from "inversify";
import * as mongoose from "mongoose";
import {Model} from "mongoose";
import * as SMTPTransport from "nodemailer/lib/smtp-transport";
import * as readline from "readline";
import {ForkQueue} from "ts-fork-queue";
import {CaptchaConfigInterface} from "../core/captcha/CaptchaConfigInterface";
import {ConnectorInterface} from "../core/db/ConnectorInterface";
import {MongoConnector} from "../core/db/MongoConnector";
import {Token} from "../resources/token/Token";
import {TokenConfigInterface} from "../resources/token/TokenConfigInterface";
import {TokenRequest} from "../resources/token/TokenRequest";
import {BuildFrontendConfig} from "../resources/webhook/hooks/buildFrontend/BuildFrontendConfig";
import {WebhookConfigEntryInterface} from "../resources/webhook/WebhookConfigEntryInterface";
import {WebhookConfigInterface} from "../resources/webhook/WebhookConfigInterface";
import {EmailTemplateFactory} from "./EmailTemplateFactory";
import {TYPES} from "./Types";

export class Module extends BaseModule {

    protected bind(container: Container): void {
        container.bind<string[]>(TYPES.ProcessArgv).toConstantValue(process.argv);

        const config = container.get(Config);

        container.bind<string>(TYPES.mongoConnectUri).toConstantValue(config.getValue(["mongo", "uri"]));
        container.bind<number>(TYPES.databaseConnectRetryDelayMs).toConstantValue(1000);

        container.bind(TYPES.ReadLine).toConstantValue(readline);
        container.bind(TYPES.Commander).toConstantValue(commander);
        const mongooseInstance: any = mongoose;
        container.bind(TYPES.mongoose).toConstantValue(mongooseInstance.default);

        const captchaConfig: CaptchaConfigInterface = config.getValue(["captcha"]);
        const tokenConfig: TokenConfigInterface = config.getValue(["resources", "token"]);
        const webhookConfig: WebhookConfigInterface = config.getValue(["resources", "webhook"]);
        const buildFrontendConfig = this.getWebhookConfig(config, "buildFrontend");
        const smptTransportOptions = config.getValue(["mail"]);

        container.bind<CaptchaConfigInterface>(TYPES.CaptchaConfigInterface).toConstantValue(captchaConfig);
        container.bind<TokenConfigInterface>(TYPES.TokenConfigInterface).toConstantValue(tokenConfig);
        container.bind<WebhookConfigInterface>(TYPES.WebhookConfigInterface).toConstantValue(webhookConfig);
        container.bind<BuildFrontendConfig>(TYPES.BuildFrontendConfig)
            .toConstantValue(buildFrontendConfig as BuildFrontendConfig);
        container.bind<SMTPTransport.Options>(TYPES.SMTPTransportOptions).toConstantValue(smptTransportOptions);

        container.bind<ForkQueue>(TYPES.BuildFrontendQueue).toConstantValue(new ForkQueue( {
            maxParallelism: 1,
            maxQueueSize: 1,
            pollingPeriodSeconds: 2,
        }));

        const connector: ConnectorInterface = container.get(MongoConnector);
        container.bind<ConnectorInterface>(TYPES.ConnectorInterface).toConstantValue(connector);

        const emailTemplate: EmailTemplate = container.get(EmailTemplateFactory).create();
        container.bind(EmailTemplate).toConstantValue(emailTemplate);

        this.declareModels(container);
    }

    private declareModels(container: Container): void {
        container.bind<Model<any>>(TYPES.TokenModel)
            .toConstantValue((new Token())
            .getModelForClass(Token));
        container.bind<Model<any>>(TYPES.TokenRequestModel)
            .toConstantValue((new TokenRequest())
            .getModelForClass(Token));
    }

    private getWebhookConfig(config: Config, webhookId: string): object | undefined {
        const hooks = config.getValue(["resources", "webhook", "hooks"]);

        const webhookConfig: WebhookConfigEntryInterface = hooks.find((entry: WebhookConfigEntryInterface) => {
            return entry.name === webhookId;
        });

        if (webhookConfig) {
            return webhookConfig.config;
        }

        return;
    }
}
