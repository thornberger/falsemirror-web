import {Logger} from "graublau";
import {inject} from "inversify";
import {provide} from "inversify-binding-decorators";
import {Model} from "mongoose";
import {TYPES} from "../../application/Types";
import {Hash} from "../../core/crypto/Hash";
import {Mailer} from "../../core/mail/Mailer";
import {RequestStatus} from "./RequestStatus";
import {Status} from "./Status";
import {TokenConfigInterface} from "./TokenConfigInterface";
import {TokenMail} from "./TokenMail";
import {TokenRequest} from "./TokenRequest";

@provide(TokenService)
export class TokenService {
    private readonly tokenRequestModel: Model<any>;
    private readonly tokenModel: Model<any>;
    private hash: Hash;
    private mailer: Mailer;
    private logger: Logger;
    private config: TokenConfigInterface;

    constructor(hash: Hash,
                mailer: Mailer,
                logger: Logger,
                @inject(TYPES.TokenConfigInterface) config: TokenConfigInterface,
                @inject(TYPES.TokenModel) tokenModel: Model<any>,
                @inject(TYPES.TokenRequestModel) tokenRequestModel: Model<any>,
    ) {
        this.hash = hash;
        this.mailer = mailer;
        this.logger = logger;
        this.config = config;
        this.tokenModel = tokenModel;
        this.tokenRequestModel = tokenRequestModel;
    }

    public async getStatus(): Promise<Status> {
        const available = await this.tokenModel.countDocuments({wasSent: false});
        const sent = await this.tokenModel.countDocuments({wasSent: true});

        return {available, sent};
    }

    public async processRequest(email: string): Promise<RequestStatus> {
        const hashedEmail = this.hashEmail(email);
        let code;

        const existingRequest = await this.getRequest(hashedEmail);
        if (!existingRequest) {
            code = await this.getNewCode();
            if (!code) {
                this.logger.warn("Tokens are depleted.");
                return RequestStatus.tokensDepleted;
            }

            await this.saveRequest(hashedEmail, code);
        } else {
            code = existingRequest.code;
            if (!code) {
                this.logger.error("Unexpected token request data.", existingRequest);
                return RequestStatus.error;
            }
        }

        return this.sendMail(email, code);
    }

    private hashEmail(email: string): string {
        return this.hash.hmac(email, this.config.hmacSecret);
    }

    private async getRequest(hashedEmail: string): Promise<TokenRequest> {
        return await this.tokenRequestModel.findOne({hashedEmail});
    }

    private async saveRequest(hashedEmail: string, code: string): Promise<void> {
        const request = new this.tokenRequestModel({hashedEmail, code});
        await request.save();
    }

    private async getNewCode(): Promise<string | null> {
        const token = await this.tokenModel.findOne({wasSent: false});

        if (!token) {
            return null;
        }

        const code = token.code;

        token.wasSent = true;
        await token.save();

        return code;
    }

    private async sendMail(email: string, code: string): Promise<RequestStatus> {
        const mail = new TokenMail(code);

        try {
            await this.mailer.send(mail, "service@falsemirror.de", [email]);
        } catch (error) {
            this.logger.error("Unable to send mail.", error);
            return RequestStatus.unableToSendMail;
        }

        return RequestStatus.ok;
    }
}
