export enum RequestStatus {
    ok = "ok",
    invalidEmail = "invalidEmail",
    missingCaptcha = "missingCaptcha",
    captchaNotSolved = "captchaNotSolved",
    unableToSendMail = "unableToSendMail",
    tokensDepleted = "tokensDepleted",
    error = "error",
}
