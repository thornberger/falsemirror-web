import {Request, Response} from "express";
import {validationResult} from "express-validator";
import {Logger} from "graublau";
import {provide} from "inversify-binding-decorators";
import {Controller} from "../../core/application/Controller";
import {Verifier} from "../../core/captcha/Verifier";
import {RequestStatus} from "./RequestStatus";
import {Status} from "./Status";
import {TokenService} from "./TokenService";

@provide(TokenController)
export class TokenController extends Controller {
    private verifier: Verifier;
    private service: TokenService;

    constructor(logger: Logger, verifier: Verifier, service: TokenService) {
        super(logger);
        this.verifier = verifier;
        this.service = service;

        this.getStatus = this.getStatus.bind(this);
        this.processRequest = this.processRequest.bind(this);
    }

    // @ts-ignore
    public getStatus(request: Request, response: Response): void {
        this.service.getStatus().then((status: Status) => {
            return response.status(200).json(status);
        }).catch((error: Error) => {
            return this.handleApplicationError(error, response);
        });
    }

    public processRequest(request: Request, response: Response): void {
        const errors = validationResult(request);
        if (!errors.isEmpty()) {
            return this.handleValidationError(errors, response);
        }

        const email = request.body.email;
        const captchaResponse = request.body.captchaResponse;
        const clientIp = request.header("x-forwarded-for") || request.connection.remoteAddress || "";

        this.verifier.isVerified(captchaResponse, clientIp).then((isVerified: boolean) => {
            if (isVerified) {
                return this.service.processRequest(email).then((status: RequestStatus) => {
                    return response.status(200).json({status});
                }).catch((error: Error) => {
                    return this.handleApplicationError(error, response);
                });
            } else {
                return response.status(403).json({status: RequestStatus.captchaNotSolved});
            }
        });
    }
}
