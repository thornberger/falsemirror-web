import {prop, Typegoose} from "typegoose";

export class TokenRequest extends Typegoose {
    @prop({ unique: true })
    public hashedEmail: string | undefined;

    @prop()
    public code: string | undefined;
}
