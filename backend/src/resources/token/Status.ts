export interface Status {
    available: number;
    sent: number;
}
