import {Router} from "express";
import {check} from "express-validator";
import {ResourceInterface} from "graublau";
import {inject} from "inversify";
import {provide} from "inversify-binding-decorators";
import {TYPES} from "../../application/Types";
import {RequestStatus} from "./RequestStatus";
import {TokenController} from "./TokenController";

@provide(TokenResource)
export class TokenResource implements ResourceInterface {
    private readonly router: Router;
    private readonly controller: TokenController;

    constructor(@inject(TYPES.Express) express: any, controller: TokenController) {
        this.router = express.Router();
        this.controller = controller;
    }

    public getRoutes(): Router {
        this.router.get("/status", this.controller.getStatus);
        this.router.post("/request",
            [check("email").isEmail().withMessage(RequestStatus.invalidEmail)],
            [check("captchaResponse").isString().withMessage(RequestStatus.missingCaptcha)],
            this.controller.processRequest);
        return this.router;
    }
}
