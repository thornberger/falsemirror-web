import {Mail} from "../../core/mail/Mail";

export class TokenMail extends Mail {
    private readonly code: string;

    constructor(code: string) {
        super();
        this.code = code;
    }

    protected getTemplate(): string {
        return "token";
    }

    protected async getVariables(): Promise<object> {
        return {
            code: this.code,
        };
    }
}
