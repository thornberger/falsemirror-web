import {Command} from "commander";
import {Logger, Module} from "graublau";
import {inject} from "inversify";
import {provide} from "inversify-binding-decorators";
import {BulkWriteOpResultObject} from "mongodb";
import {Model} from "mongoose";
import {ReadLine} from "readline";
import {TYPES} from "../../application/Types";
import {Script} from "../../core/application/Script";

@provide(TokenImportScript)
export class TokenImportScript implements Script {
    private command: Command;
    private readLine: any;
    private readonly argv: string[];
    private readonly tokenModel: Model<any>;
    private fileSystem: any;
    private logger: Logger;

    protected constructor(@inject(TYPES.Commander) commander: any,
                          @inject(Module.Types.FileSystem) fileSystem: any,
                          @inject(TYPES.ReadLine) readLine: ReadLine,
                          @inject(TYPES.ProcessArgv) argv: string[],
                          @inject(TYPES.TokenModel) tokenModel: Model<any>,
                          logger: Logger) {
        this.fileSystem = fileSystem;
        this.command = new commander.Command();
        this.readLine = readLine;
        this.argv = argv;
        this.tokenModel = tokenModel;
        this.logger = logger;

        this.insert = this.insert.bind(this);

        this.initializeCommand();
    }

    public async run(): Promise<void> {
        const filePath = this.getFilePath();
        const codes = await this.readCodesFromFile(filePath);

        return this.insert(codes);
    }

    private initializeCommand(): void {
        // noinspection HtmlUnknownTag
        this.command.arguments("<file>");
    }

    private getFilePath(): string {
        const parsed = this.command.parse(this.argv);

        const file = parsed.args[1];
        if (!file) {
            const message = "No file provided.";
            throw new Error(message);
        }

        return file;
    }

    private async readCodesFromFile(path: string): Promise<string[]> {
        return new Promise<string[]>(((resolve, reject) => {
            const readLine: any = this.readLine.createInterface({
                input: this.fileSystem.createReadStream(path),
            });
            const codes: string[] = [];

            readLine.on("line", (code: string) => {
                codes.push(code);
            }).on("error", (error: Error) => {
                reject(error);
            }).on("close", () => {
                resolve(codes);
            });
        }));
    }

    private async insert(codes: string[]): Promise<void> {
        const writes = codes.map((code: string) => {
            return {
                updateOne: {
                    filter: {
                        code,
                    },
                    update: {
                        code,
                        wasSent: false,
                    },
                    upsert: true,
                },

            };
        });

        return new Promise<void>(((resolve, reject) => {
            return this.tokenModel.bulkWrite(writes, (error: Error, result: BulkWriteOpResultObject) => {
                if (error) {
                    reject(error);
                }

                this.logger.info(`Added ${result.upsertedCount}, updated ${result.modifiedCount} codes`);
                resolve();
            });
        }));
    }

}
