import {prop, Typegoose} from "typegoose";

export class Token extends Typegoose {

    @prop({unique: true, required: true})
    public code: string | undefined;

    @prop({index: true, default: false})
    public wasSent: boolean | undefined;
}
