export interface HookInterface {
    run(): Promise<void>;
}
