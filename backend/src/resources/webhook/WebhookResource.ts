import {Router} from "express";
import {check} from "express-validator";
import {ResourceInterface} from "graublau";
import {inject} from "inversify";
import {provide} from "inversify-binding-decorators";
import {TYPES} from "../../application/Types";
import {WebhookController} from "./WebhookController";

@provide(WebhookResource)
export class WebhookResource implements ResourceInterface {
    private readonly router: Router;
    private readonly controller: WebhookController;

    constructor(@inject(TYPES.Express) express: any, controller: WebhookController) {
        this.router = express.Router();
        this.controller = controller;
    }

    public getRoutes(): Router {
        this.router.post("/:id",
            [check("id").isString().withMessage("invalidOrMissingWorkerId")],
            this.controller.run);
        return this.router;
    }
}
