import {spawn, SpawnOptions} from "child_process";
import {Logger} from "graublau";
import {inject} from "inversify";
import {provide} from "inversify-binding-decorators";
import * as path from "path";
import requestPromise from "request-promise-native";
import {TYPES} from "../../../../application/Types";
import {Script} from "../../../../core/application/Script";
import {BuildFrontendConfig} from "./BuildFrontendConfig";
import {CachePurgeResponseInterface} from "./CachePurgeResponseInterface";

@provide(BuildFrontendScript)
export class BuildFrontendScript implements Script {

    private static readonly FLASH_CACHE_URL =
        "https://api.cloudflare.com/client/v4/zones/666de4650d8f5575e76b3eb7aa459d60/purge_cache";
    private static readonly LOGGER_PREFIX = "[BUILD FRONTEND] ";

    private config: BuildFrontendConfig;
    private logger: Logger;

    constructor(
        @inject(TYPES.BuildFrontendConfig) config: BuildFrontendConfig,
        logger: Logger,
    ) {
        this.config = config;
        this.logger = logger;

        this.buildFrontend = this.buildFrontend.bind(this);
        this.flushCache = this.flushCache.bind(this);
    }

    public async run(): Promise<void> {
        return this.buildFrontend()
            .then(this.flushCache);
    }

    private async buildFrontend(): Promise<void> {
        const command = this.getCommand();
        const options = this.getOptions();

        return new Promise<void>(((resolve, reject) => {
            const childProcess = spawn(command, [], options);

            if (childProcess.stdout !== null) {
                childProcess.stdout.on("data", (data: string) => {
                    this.logger.info(BuildFrontendScript.LOGGER_PREFIX + data);
                });
            }

            if (childProcess.stderr !== null) {
                childProcess.stderr.on("data", (data: string) => {
                    this.logger.warn(BuildFrontendScript.LOGGER_PREFIX + data);
                });
            }
            childProcess.on("close", (code: number) => {
                if (code > 0) {
                    const message = BuildFrontendScript.LOGGER_PREFIX + "Build frontend script exited with code " +
                        code;
                    this.logger.error(message);

                    return reject();
                }
                this.logger.info(BuildFrontendScript.LOGGER_PREFIX + "Build frontend script finished");
                return resolve();
            });
        }));
    }

    private async flushCache(): Promise<void> {
        const options = {
            body: {
                purge_everything: true,
            },
            headers: {
                "X-Auth-Email": this.config.cloudFlareEmail,
                "X-Auth-Key": this.config.cloudFlareAuthKey,
            },
            json: true,
            method: "DELETE",
            url: BuildFrontendScript.FLASH_CACHE_URL,
        };

        try {
            const response: CachePurgeResponseInterface = await requestPromise(options);
            if (!response.success && response.errors.length > 0) {
                response.errors.forEach((error: { code: number, message: string }) => {
                    this.logger.error(`${BuildFrontendScript.LOGGER_PREFIX} Error: ${error.code} ${error.message}`);
                });
            } else {
                this.logger.info("Response from CloudFlare.", response);
            }
        } catch (error) {
            this.logger.error(error);
        }
    }

    private getCommand(): string {
        return this.config.script;
    }

    private getOptions(): SpawnOptions {
        return {
            cwd: this.getCwd(),
        };
    }

    private getCwd(): string {
        return path.dirname(this.config.script);
    }

}
