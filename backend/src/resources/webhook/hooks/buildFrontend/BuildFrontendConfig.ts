export interface BuildFrontendConfig {
    script: string;
    cloudFlareEmail: string;
    cloudFlareAuthKey: string;
}
