export interface CachePurgeResponseInterface {
    success: boolean;
    errors: [{
        code: number;
        message: string;
    }];

    [key: string]: any;
}
