import {Logger} from "graublau";
import {inject} from "inversify";
import {provide} from "inversify-binding-decorators";
import {ForkQueue} from "ts-fork-queue";
import {TYPES} from "../../../../application/Types";
import {HookInterface} from "../../HookInterface";
import {ScriptTask} from "../../queue/ScriptTask";

@provide(BuildFrontendWebhook)
export class BuildFrontendWebhook implements HookInterface {

    private static readonly SCRIPT = "BuildFrontend";

    private queue: ForkQueue;
    private logger: Logger;

    public constructor(@inject(TYPES.BuildFrontendQueue) queue: ForkQueue, logger: Logger) {
        this.queue = queue;
        this.logger = logger;

        this.queue.start();
    }

    public async run(): Promise<void> {
        const task = this.createTask();
        try {
            this.queue.enqueue(task);
        } catch (error) {
            this.logger.info("Frontend build queue is full.");
        }
    }

    private createTask(): ScriptTask {
        return new ScriptTask(BuildFrontendWebhook.SCRIPT);
    }
}
