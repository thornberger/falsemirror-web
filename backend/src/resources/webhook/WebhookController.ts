import {Request, Response} from "express";
import {validationResult} from "express-validator";
import {Logger} from "graublau";
import {provide} from "inversify-binding-decorators";
import {Controller} from "../../core/application/Controller";
import {WebhookError} from "./errors/WebhookError";
import {WebhookService} from "./WebhookService";

@provide(WebhookController)
export class WebhookController extends Controller {
    private static readonly ACCESS_TOKEN_HEADER = "x-fm-webhook-access-token";

    private service: WebhookService;

    constructor(logger: Logger, service: WebhookService) {
        super(logger);

        this.service = service;
        this.run = this.run.bind(this);
    }

    public run(request: Request, response: Response): void {
        const errors = validationResult(request);
        if (!errors.isEmpty()) {
            return this.handleValidationError(errors, response);
        }

        const id = request.params.id;
        const accessToken = request.header(WebhookController.ACCESS_TOKEN_HEADER);

        this.service.run(id, accessToken)
            .then(() => {
                return response.status(204).send();
            })
            .catch((error: Error) => {
                if (error instanceof WebhookError) {
                    return response.status(403).json({status: error.message});
                }

                return this.handleApplicationError(error, response);
            });
    }
}
