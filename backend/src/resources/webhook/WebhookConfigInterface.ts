import {WebhookConfigEntryInterface} from "./WebhookConfigEntryInterface";

export interface WebhookConfigInterface {
    hooks: WebhookConfigEntryInterface[];
}
