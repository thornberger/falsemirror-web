import {ForkQueueTask} from "ts-fork-queue";

export class ScriptTask implements ForkQueueTask {

    private static readonly COMMAND = "dist/cli.js";

    private readonly script: string;
    private readonly scriptArguments: string[] | undefined;

    public constructor(script: string, scriptArguments?: string[]) {
        this.script = script;
        this.scriptArguments = scriptArguments;
    }

    public getCommand(): string {
        return ScriptTask.COMMAND;
    }

    public getArgs(): string[] {
        let args = [this.script];

        if (this.scriptArguments) {
            args = args.concat(this.scriptArguments);
        }

        return args;
    }

    public getCwd(): string {
        return __dirname + "/../../../../";
    }

    public toString(): string {
        return this.getCommand() + " " + this.getArgs();
    }

}
