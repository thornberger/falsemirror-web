import {inject} from "inversify";
import {provide} from "inversify-binding-decorators";
import {TYPES} from "../../application/Types";
import {WebhookAccessTokenInvalidError} from "./errors/WebhookAccessTokenInvalidError";
import {WebhookAccessTokenMissingError} from "./errors/WebhookAccessTokenMissingError";
import {WebhookNotRegisteredError} from "./errors/WebhookNotRegisteredError";
import {HookInterface} from "./HookInterface";
import {BuildFrontendWebhook} from "./hooks/buildFrontend/BuildFrontendWebhook";
import {WebhookConfigEntryInterface} from "./WebhookConfigEntryInterface";
import {WebhookConfigInterface} from "./WebhookConfigInterface";

@provide(WebhookService)
export class WebhookService {

    private config: WebhookConfigInterface;
    private hooks: { [id: string]: HookInterface } = {};

    constructor(@inject(TYPES.WebhookConfigInterface) config: WebhookConfigInterface,
                buildFrontendWebhook: BuildFrontendWebhook) {
        this.config = config;

        this.registerHook("buildFrontend", buildFrontendWebhook);
    }

    public async run(id: string, accessToken?: string): Promise<void> {
        this.validate(id, accessToken);

        const worker = this.getHook(id);

        return await worker.run();
    }

    private validate(id: string, accessToken?: string): void {
        if (!accessToken) {
            throw new WebhookAccessTokenMissingError(id);
        }

        if (!this.isHookRegistered(id)) {
            throw new WebhookNotRegisteredError(id);
        }

        const configEntry = this.getConfigEntry(id);

        if (!configEntry) {
            throw new WebhookNotRegisteredError(id);
        }

        if (configEntry.accessToken !== accessToken) {
            throw new WebhookAccessTokenInvalidError(id);
        }

    }

    private registerHook(id: string, instance: HookInterface): void {
        this.hooks[id] = instance;
    }

    private getHook(id: string): HookInterface {
        return this.hooks[id];
    }

    private isHookRegistered(id: string): boolean {
        return id in this.hooks;
    }

    private getConfigEntry(id: string): WebhookConfigEntryInterface | undefined {
        return this.config.hooks.find((entry: WebhookConfigEntryInterface) => {
            return entry.name === id;
        });
    }
}
