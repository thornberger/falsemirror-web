export interface WebhookConfigEntryInterface {
    name: string;
    accessToken: string;
    config: object;
}
