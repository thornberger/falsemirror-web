import {WebhookError} from "./WebhookError";

export class WebhookAccessTokenInvalidError extends WebhookError {
    constructor(id: string) {
        super(`The access token for webhook ${id} could not be validated.`);
    }
}
