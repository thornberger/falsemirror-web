import {WebhookError} from "./WebhookError";

export class WebhookNotRegisteredError extends WebhookError {
    constructor(id: string) {
        super(`Webhook ${id} is not configured.`);
    }
}
