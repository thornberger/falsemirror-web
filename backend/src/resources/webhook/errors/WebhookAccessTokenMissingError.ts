import {WebhookError} from "./WebhookError";

export class WebhookAccessTokenMissingError extends WebhookError {
    constructor(id: string) {
        super(`Missing access token in request for webhook ${id}.`);
    }
}
