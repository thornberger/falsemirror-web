"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Crypto = require("crypto");
var inversify_binding_decorators_1 = require("inversify-binding-decorators");
var Hash = /** @class */ (function () {
    function Hash() {
    }
    Hash_1 = Hash;
    // noinspection JSMethodCanBeStatic
    Hash.prototype.hmac = function (message, secret) {
        var hmac = Crypto.createHmac(Hash_1.ALGORITHM, secret);
        hmac.update(message);
        return hmac.digest(Hash_1.OUTPUT_FORMAT);
    };
    var Hash_1;
    Hash.ALGORITHM = "sha256";
    Hash.OUTPUT_FORMAT = "hex";
    Hash = Hash_1 = __decorate([
        inversify_binding_decorators_1.provide(Hash_1)
    ], Hash);
    return Hash;
}());
exports.Hash = Hash;
