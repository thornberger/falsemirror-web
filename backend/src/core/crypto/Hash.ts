import * as Crypto from "crypto";
import {provide} from "inversify-binding-decorators";

@provide(Hash)
export class Hash {
    private static readonly ALGORITHM = "sha256";
    private static readonly OUTPUT_FORMAT = "hex";

    public hmac(message: string, secret: string): string {
        const hmac = Crypto.createHmac(Hash.ALGORITHM, secret);
        hmac.update(message);
        return hmac.digest(Hash.OUTPUT_FORMAT);
    }
}
