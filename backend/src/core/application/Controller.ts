import {Response} from "express";
import {Result} from "express-validator";
import {Logger} from "graublau";
import {injectable} from "inversify";

@injectable()
export abstract class Controller {
    private logger: Logger;

    protected constructor(logger: Logger) {
        this.logger = logger;
    }

    protected handleValidationError(errors: Result<any>, response: Response): void {
        const firstError = errors.array({onlyFirstError: true})[0];
        response.status(400).json({status: firstError.msg});
    }

    protected handleApplicationError(error: Error, response: Response): void {
        this.logger.error(error);
        response.status(500).json({error: error.message});
    }
}
