export interface MailOptionsInterface {

    template: string;

    message: any;

    locals: any;
}
