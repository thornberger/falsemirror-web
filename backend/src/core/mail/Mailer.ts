import EmailTemplate = require("email-templates");
import {provide} from "inversify-binding-decorators";
import {Mail} from "./Mail";

@provide(Mailer)
export class Mailer {
    private emailTemplate: EmailTemplate;

    constructor(emailTemplates: EmailTemplate) {
        this.emailTemplate = emailTemplates;
    }

    public async send(mail: Mail, sender: string, recipients: string[]): Promise<void> {
        const data = await mail.getData(sender, recipients);

        return this.emailTemplate.send(data);
    }
}
