import {provide} from "inversify-binding-decorators";
import {MailOptionsInterface} from "./MailOptionsInterface";

@provide(Mail)
export abstract class Mail {
    public async getData(sender: string, recipients: string[]): Promise<MailOptionsInterface> {
        const locals = await this.getVariables();
        const template = this.getTemplate();

        return {
            locals,
            message: {
                from: sender,
                to: recipients,
            },
            template,
        };
    }

    protected abstract getTemplate(): string;

    protected abstract async getVariables(): Promise<object>;
}
