import {inject} from "inversify";
import {provide} from "inversify-binding-decorators";
import {TYPES} from "../../application/Types";
import {ConnectorInterface} from "./ConnectorInterface";

@provide(MongoConnector)
export class MongoConnector implements ConnectorInterface {
    private static readonly NAME = "Mongo";

    // @ts-ignore
    private readonly mongoose: any;
    private readonly connectUri: string;

    public constructor(@inject(TYPES.mongoose) mongoose: any, @inject(TYPES.mongoConnectUri) connectUri: string) {
        this.mongoose = mongoose;
        this.connectUri = connectUri;
    }

    public async connect(): Promise<any> {
        return this.mongoose.connect(this.connectUri, {useNewUrlParser: true, useCreateIndex: true});
    }

    public toString(): string {
        return `${MongoConnector.NAME} ${this.connectUri}`;
    }
}
