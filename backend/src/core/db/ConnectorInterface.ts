export interface ConnectorInterface {

    connect(): Promise<any>;

    toString(): string;
}
