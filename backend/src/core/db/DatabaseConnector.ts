import {Logger} from "graublau";
import {inject} from "inversify";
import {provide} from "inversify-binding-decorators";
import {TYPES} from "../../application/Types";
import {ConnectorInterface} from "./ConnectorInterface";

@provide(DatabaseConnector)
export class DatabaseConnector {
    public static readonly MAX_ATTEMPTS = 5;

    private connector: ConnectorInterface;
    private readonly retryDelayMs: number;
    private logger: Logger;

    public constructor(@inject(TYPES.ConnectorInterface) connector: ConnectorInterface,
                       @inject(TYPES.databaseConnectRetryDelayMs) retryDelayMs: number,
                       logger: Logger) {
        this.connector = connector;
        this.retryDelayMs = retryDelayMs;
        this.logger = logger;

        this.connect = this.connect.bind(this);
        this.connectWithRetries = this.connectWithRetries.bind(this);
    }

    public connect(): Promise<void> {
        return this.connectWithRetries(1, DatabaseConnector.MAX_ATTEMPTS);
    }

    private connectWithRetries(attempts: number, limit: number): Promise<void> {
        return this.connector.connect()
            .then(() => {
                this.logger.info("Connected to database " + this.connector.toString());
            })
            .catch(async (err: Error) => {
                this.logger.error(err);
                if (attempts < limit) {
                    setTimeout(async () => {
                        return this.connectWithRetries(attempts + 1, limit);
                    }, this.retryDelayMs);
                } else {
                    this.logger.error(`Database connection failed after ${attempts} attempts.`, err);
                }
            });
    }
}
