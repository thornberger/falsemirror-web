import {inject} from "inversify";
import {provide} from "inversify-binding-decorators";
import requestPromise from "request-promise-native";
import {TYPES} from "../../application/Types";
import {CaptchaConfigInterface} from "./CaptchaConfigInterface";
import {VerificationResponseInterface} from "./VerificationResponseInterface";

@provide(Verifier)
export class Verifier {
    private static readonly VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify";

    private config: CaptchaConfigInterface;

    public constructor(@inject(TYPES.CaptchaConfigInterface) config: CaptchaConfigInterface) {
        this.config = config;
    }

    public async isVerified(captchaResponse: string, clientIp: string): Promise<boolean> {
        const options = {
            json: true,
            method: "POST",
            url: Verifier.VERIFY_URL +
                "?response=" + captchaResponse +
                "&secret=" + this.config.secret +
                "&remoteip=" + clientIp,
        };

        try {
            const response: VerificationResponseInterface = await requestPromise(options);
            return response.success;
        } catch (error) {
            return false;
        }
    }

}
