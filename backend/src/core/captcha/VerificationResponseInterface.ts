export interface VerificationResponseInterface {
    success: boolean;

    [key: string]: any;
}
