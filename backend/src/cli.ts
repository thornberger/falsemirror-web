import "reflect-metadata";
import {CliApplication} from "./application/CliApplication";
import {Module} from "./application/Module";

const application = new CliApplication(new Module(__dirname + "/../config/config.json"));
application.run();
