import "reflect-metadata";
import {Module} from "./application/Module";
import {WebApplication} from "./application/WebApplication";

const application = new WebApplication(new Module(__dirname + "/../config/config.json"));
application.run();
