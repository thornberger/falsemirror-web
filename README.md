# The False Mirror Website

## Local Installation
* Create a `config.json` file in `/backend/config`.
* Create a `.env` files in `/frontend` with the secret environment variables.
* Run `yarn install` for backend und frontend.

## Run locally
Run `yarn dev` from `/backend`.
