const path = require(`path`)

const PAGES = ['index', 'live', 'about', 'legal'];

exports.createPages = ({graphql, actions}) => {
    const {createPage} = actions
    return new Promise((resolve, reject) => {
        PAGES.forEach((name) => {
            let slug;
            if (name === 'index') {
                slug = '/';
            } else {
                slug = `/${name}`;
            }

            createPage({
                path: slug,
                component: path.resolve('./src/templates/PageTemplate.tsx'),
                context: {
                    name: name,
                },
            })
        });
        resolve();
    })
}
