import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Image} from "./Image";
import {ImageType} from "./ImageType";

it('renders', () => {
    const div = document.createElement('div');
    ReactDOM.render(
        <Image
            name="foo"
            type={ImageType.fixed}
            data={
                {
                    height: 200,
                    src: "foo",
                    srcSet: "foo",
                    width: 200
                }
            }
        />, div);
    ReactDOM.unmountComponentAtNode(div);
});
