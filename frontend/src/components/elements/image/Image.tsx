import GatsbyImage, {FixedObject, FluidObject} from "gatsby-image";
import * as React from "react";
import {ReactNode} from "react";
import {ImageProps} from "./ImageProps";
import {ImageType} from "./ImageType";

export class Image extends React.Component<ImageProps> {

    public constructor(props: ImageProps) {
        super(props);

        this.renderFixed = this.renderFixed.bind(this);
        this.renderFluid = this.renderFluid.bind(this);
    }

    public render(): ReactNode {
        if (this.props.type === ImageType.fixed) {
            return this.renderFixed();
        }

        return this.renderFluid();
    }

    private renderFixed(): ReactNode {
        return (
            <GatsbyImage
                fixed={this.props.data as FixedObject}
                className={this.props.className}
                alt={this.props.name}
                title={this.props.name}
                imgStyle={this.props.imageStyle}
            />
        );
    }

    private renderFluid(): ReactNode {
        return (
            <GatsbyImage
                fluid={this.props.data as FluidObject}
                className={this.props.className}
                alt={this.props.name}
                title={this.props.name}
                imgStyle={this.props.imageStyle}
            />
        );
    }
}
