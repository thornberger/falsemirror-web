import {ImageData} from "./ImageData";
import {ImageType} from "./ImageType";

export interface ImageProps {
    name: string,
    type: ImageType,
    data: ImageData,
    imageStyle?: object,
    className?: string
}
