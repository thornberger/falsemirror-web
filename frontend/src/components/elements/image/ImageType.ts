export enum ImageType {
    fluid = "fluid",
    fixed = "fixed"
}
