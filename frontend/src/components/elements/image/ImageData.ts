export interface ImageData {
    base64?: string | null;
    tracedSVG?: string | null;
    aspectRatio?: number | null;
    width?: number | null;
    height?: number | null;
    src?: string | null;
    srcSet?: string | null;
    srcWebp?: string | null;
    srcSetWebp?: string | null;
    sizes?: string | null;
    originalImg?: string | null;
    originalName?: string | null;
    presentationWidth?: number | null;
    presentationHeight?: number | null;
}
