import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Progress from "./Progress";

it('renders', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Progress value={0.2} colorFill="red" colorEmpty="green" />, div);
  ReactDOM.unmountComponentAtNode(div);
});
