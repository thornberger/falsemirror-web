export interface ProgressProps {
    value: number;
    colorFill: string;
    colorEmpty: string;
}
