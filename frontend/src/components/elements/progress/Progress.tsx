import * as React from "react";
import {ReactNode} from "react";
import './Progress.css';
import {ProgressProps} from "./ProgressProps";

class Progress extends React.Component<ProgressProps> {

    constructor(props: ProgressProps){
        super(props);
    }

    public render(): ReactNode {
        const cssWidth = this.getCSSWidth();

        return (
            <div className="Progress" style={{backgroundColor: this.props.colorEmpty}}>
                <div style={{backgroundColor: this.props.colorFill, width: cssWidth}} />
            </div>
        );
    }

    private getCSSWidth(): string {
        let percentage = 100 * Math.min(1.0, Math.max(0, this.props.value));
        percentage = Math.floor(percentage);

        return percentage + "%";
    }
}

export default Progress;
