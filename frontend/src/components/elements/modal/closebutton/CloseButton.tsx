import {faTimesCircle} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import * as React from "react";
import {ReactNode} from "react";
import './CloseButton.css';
import {CloseButtonProps} from "./CloseButtonProps";

class CloseButton extends React.Component<CloseButtonProps> {

    public render(): ReactNode {
        return (
            <span className="CloseButton" onClick={this.props.onClick}>
                <FontAwesomeIcon icon={faTimesCircle}/>
            </span>
        );
    }
}

export default CloseButton;
