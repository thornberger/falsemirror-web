import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Button from "./Button";

it('renders', () => {

    const div = document.createElement('div');
    ReactDOM.render(<Button label="foo" isDisabled={false} onClick={console.log}/>, div);
    ReactDOM.unmountComponentAtNode(div);
});
