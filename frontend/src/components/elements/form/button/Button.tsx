import * as React from "react";
import {ReactNode} from "react";
import './Button.css';
import {ButtonProps} from "./ButtonProps";

class Button extends React.Component<ButtonProps> {

    public render(): ReactNode {
        return (
            <input
                className="Button"
                type="button"
                value={this.props.label}
                disabled={this.props.isDisabled}
                onClick={this.props.onClick}
            />
        );
    }
}

export default Button;
