import * as React from "react";
import {ReactNode} from "react";
import './TextField.css';
import {TextFieldProps} from "./TextFieldProps";

class TextField extends React.Component<TextFieldProps> {

    public render(): ReactNode {
        return (
            <div className="TextField">
                <label>{this.props.label}
                    <input className="TextField" type="text" value={this.props.value}
                           onChange={this.props.onChange}/>
                </label>
            </div>
        );
    }
}

export default TextField;
