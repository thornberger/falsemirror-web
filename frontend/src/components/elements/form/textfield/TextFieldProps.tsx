import {ChangeEvent} from "react";

export interface TextFieldProps {
    label: string;
    value: string;
    onChange: (event: ChangeEvent<HTMLInputElement>) => void;
}
