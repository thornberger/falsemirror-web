import * as React from 'react';
import {ChangeEvent} from "react";
import * as ReactDOM from 'react-dom';
import TextField from "./TextField";

it('renders', () => {
    const onChange = (event: ChangeEvent<HTMLInputElement>) => {
        console.log(event);
    };

    const div = document.createElement('div');
    ReactDOM.render(<TextField label="foo" value="bar" onChange={onChange}/>, div);
    ReactDOM.unmountComponentAtNode(div);
});
