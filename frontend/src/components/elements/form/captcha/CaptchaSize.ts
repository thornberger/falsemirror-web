export enum CaptchaSize {
    compact = "compact",
    normal = "normal",
    invisible = "invisible"
}
