import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Captcha from "./Captcha";

it('renders', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Captcha handleCaptchaSolved={console.log}/>, div);
    ReactDOM.unmountComponentAtNode(div);
});
