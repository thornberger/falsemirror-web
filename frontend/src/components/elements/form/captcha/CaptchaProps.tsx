export interface CaptchaProps {
    handleCaptchaSolved: (captchaResponse: string | null) => void;
}
