import * as React from "react";
import {ReactNode} from "react";
import ReCAPTCHA from "react-google-recaptcha";
import ScreenWidth from "../../screenwidth/ScreenWidth";
import {CaptchaProps} from "./CaptchaProps";
import {CaptchaSize} from "./CaptchaSize";
import {CaptchaState} from "./CaptchaState";

class Captcha extends React.Component<CaptchaProps, CaptchaState> {

    private static readonly SITE_KEY = "6LcVKl4UAAAAAKZDgUJqVeYoCW5P9YWTwOo2Em68";
    private static readonly MIN_VIEWPORT_WIDTH = 500;

    constructor(props: CaptchaProps) {
        super(props);
        this.state = {
            size: CaptchaSize.normal
        };
        this.handleScreenWidthChange = this.handleScreenWidthChange.bind(this);
    }

    public render(): ReactNode {
        return (
            <div className="Captcha">
                <ScreenWidth onWidthChanged={this.handleScreenWidthChange}/>
                <ReCAPTCHA
                    sitekey={Captcha.SITE_KEY}
                    onChange={this.props.handleCaptchaSolved}
                    size={this.state.size}
                />
            </div>
        );
    }

    private handleScreenWidthChange(width: number | null): void {
        let size: CaptchaSize = CaptchaSize.normal;

        if (width !== null && width < Captcha.MIN_VIEWPORT_WIDTH) {
            size = CaptchaSize.compact;
        }

        this.setState({size});
    }
}

export default Captcha;
