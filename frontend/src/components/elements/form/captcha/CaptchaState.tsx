import {CaptchaSize} from "./CaptchaSize";

export interface CaptchaState {
    size: CaptchaSize;
}
