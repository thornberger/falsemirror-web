import * as React from 'react';
import {ChangeEvent} from "react";
import * as ReactDOM from 'react-dom';
import CheckBox from "./CheckBox";

it('renders', () => {
    const onChange = (event: ChangeEvent<HTMLInputElement>) => {
        console.log(event);
    };

    const div = document.createElement('div');
    ReactDOM.render(<CheckBox label="foo" value={false} onChange={onChange}/>, div);
    ReactDOM.unmountComponentAtNode(div);
});
