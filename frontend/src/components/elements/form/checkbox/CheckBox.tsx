import * as React from "react";
import {ReactNode} from "react";
import './CheckBox.css';
import {CheckBoxProps} from "./CheckBoxProps";

class CheckBox extends React.Component<CheckBoxProps> {

    public render(): ReactNode {
        return (
            <div>
                <label className="Checkbox">{this.props.label}
                    <input
                        type="checkbox"
                        checked={this.props.value}
                        onChange={this.props.onChange}
                    />
                    <span className="Checkmark"/>
                </label>
            </div>
        );
    }
}

export default CheckBox;
