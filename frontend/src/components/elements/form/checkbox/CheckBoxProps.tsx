import {ChangeEvent} from "react";

export interface CheckBoxProps {
    label: string;
    value: boolean;
    onChange: (event: ChangeEvent<HTMLInputElement>) => void;
}
