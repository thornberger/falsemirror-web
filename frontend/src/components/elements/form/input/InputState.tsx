export interface InputState {
    name: string;
    isValid: boolean;
}
