import * as React from "react";
import {ReactNode} from "react";
import {Image} from "../image/Image";
import {ImageType} from "../image/ImageType";
import "./Content.css";
import {ContentProps} from "./ContentProps";

class Content extends React.Component<ContentProps> {

    public constructor(props: ContentProps) {
        super(props);

        this.getInnerElements = this.getInnerElements.bind(this);

    }

    public render(): ReactNode {
        const innerElements = this.getInnerElements();

        return(
            <div className="Content">
                {innerElements}
            </div>
        );
    }

    private getInnerElements(): ReactNode[] {
        const elements: ReactNode[] = [];
        if(this.props.titleImage){
            elements.push(
                <Image
                    key="image"
                    name="Title Image"
                    type={ImageType.fluid}
                    data={this.props.titleImage}
                    className="ContentTitleImage"
                />)
        }
        elements.push(<div key="html" dangerouslySetInnerHTML={{__html: this.props.text}}/>);

        return elements;
    }
}

export default Content;
