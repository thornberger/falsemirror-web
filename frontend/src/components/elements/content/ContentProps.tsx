export interface ContentProps {
    text: string;
    titleImage?: ImageData;
    additionalImages?: ImageData[];
}
