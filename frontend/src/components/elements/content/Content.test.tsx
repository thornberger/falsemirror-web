import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Content from "./Content";

it('renders', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Content text="foo" />, div);
  ReactDOM.unmountComponentAtNode(div);
});
