export interface ScreenWidthProps {
    onWidthChanged: (width: number | null) => void;
}
