import {ReactNode} from "react";
import * as React from "react";
import {ScreenWidthProps} from "./ScreenWidthProps";

class ScreenWidth extends React.Component<ScreenWidthProps> {
    constructor(props: ScreenWidthProps) {
        super(props);

        this.updateWidth = this.updateWidth.bind(this);
        this.getScreenWidth = this.getScreenWidth.bind(this);
        this.getWindowWidth = this.getWindowWidth.bind(this);
        this.getDocumentWidth = this.getDocumentWidth.bind(this);
        this.getBodyWidth = this.getBodyWidth.bind(this);
    }

    public componentWillMount(): void {
        this.updateWidth();
    }

    public componentDidMount(): void {
        window.addEventListener("resize", this.updateWidth);
    }

    public componentWillUnmount(): void {
        window.removeEventListener("resize", this.updateWidth);
    }

    public render(): ReactNode {
        return null;
    }

    private updateWidth(): void {
        const width = this.getScreenWidth();
        this.props.onWidthChanged(width);
    }

    private getScreenWidth(): number | null {
        const windowWidth = this.getWindowWidth();
        const documentWidth = this.getDocumentWidth();
        const bodyWidth = this.getBodyWidth();

        if (windowWidth) {
            return windowWidth;
        }
        if (documentWidth) {
            return documentWidth;
        }
        if (bodyWidth) {
            return bodyWidth;
        }

        return null;
    }

    private getWindowWidth(): number | null {
        if (typeof window !== 'undefined' && window.innerWidth) {
            return window.innerWidth;
        }

        return null;
    }

    private getDocumentWidth(): number | null {
        if (typeof document !== 'undefined' && document.documentElement && document.documentElement.clientWidth) {
            return document.documentElement.clientWidth;
        }

        return null;
    }

    private getBodyWidth(): number | null {
        if (typeof document !== 'undefined') {

            const body = document.getElementsByTagName('body')[0];
            if (body.clientWidth) {
                return body.clientWidth;
            }
        }

        return null;
    }
}

export default ScreenWidth;
