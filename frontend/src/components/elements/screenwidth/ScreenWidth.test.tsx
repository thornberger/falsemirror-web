import * as React from 'react';
import * as ReactDOM from 'react-dom';
import ScreenWidth from "./ScreenWidth";

it('renders', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ScreenWidth onWidthChanged={console.log}/>, div);
    ReactDOM.unmountComponentAtNode(div);
});
