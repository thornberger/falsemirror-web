import * as React from 'react';
import {ReactNode} from 'react';
import {ResultStatus} from "../elements/common/ResultStatus";
import TokenDisclaimer from "../elements/disclaimer/TokenDisclaimer";
import TokenForm from "../elements/form/TokenForm";
import TokenRemaining from "../elements/remaining/TokenRemaining";
import TokenResult from "../elements/result/TokenResult";
import './Token.css';
import {TokenProps} from "./TokenProps";
import {TokenState} from "./TokenState";
import {TokenStatusState} from "./TokenStatusState";

class Token extends React.Component<TokenProps, TokenState> {

    private static readonly UPDATE_INTERVAL_MS = 10000;

    public state: TokenState;

    private interval: NodeJS.Timer | undefined;

    constructor(props: TokenProps) {
        super(props);
        this.state = {
            result: null,
            status: {
                available: 0,
                hasData: false,
                sent: 0
            },
        };

        this.updateStatus = this.updateStatus.bind(this);
        this.handleResult = this.handleResult.bind(this);
    }

    public componentDidMount(): void {
        this.updateStatus();
        this.interval = setInterval(() => this.updateStatus(), Token.UPDATE_INTERVAL_MS);
    }

    public componentWillUnmount(): void {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    public render(): ReactNode {
        if (this.state.result !== null) {
            return <TokenResult result={this.state.result}/>
        }

        if (false && !this.props.isEnabled) {
            return this.renderDisabled();
        }

        if (this.state.status.hasData && this.state.status.available === 0) {
            return this.renderDepleted();
        }

        return this.renderEnabled();
    }

    // noinspection JSMethodCanBeStatic
    private renderDisabled(): ReactNode {
        return (
            <div className="Token">
                <h2>Congratulations</h2>
                <p>
                    You have found the bonus content for SIGINT. Unfortunately the download is not available yet. Please come back here in a
                    few days.
                </p>
            </div>
        );
    }

    private renderDepleted(): ReactNode {
        return (
            <div className="Token">
                <h2>Congratulations</h2>
                <p>
                    You have found the bonus content for SIGINT. Unfortunately all download codes have been used up. Please follow
                    False Mirror on <a href="https://facebook.com/falsemirror.de">Facebook</a> to get notified when new codes are available.
                </p>
                <TokenRemaining available={this.state.status.available} total={this.state.status.available + this.state.status.sent}/>
            </div>
        );
    }

    private renderEnabled(): ReactNode {
        return (
            <div className="Token">
                <h2>Congratulations</h2>
                <p>
                    You have found the bonus content for SIGINT. Please enter your e-mail below and you will receive a download code.
                </p>
                <TokenRemaining available={this.state.status.available} total={this.state.status.available + this.state.status.sent}/>
                <TokenForm onResult={this.handleResult}/>
                <TokenDisclaimer/>
            </div>
        );
    }

    private handleResult(result: ResultStatus): void {
        this.setState({result});
    }

    private updateStatus(): void {
        fetch('/api/token/status', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'GET',
        })
            .then((response: Response) => {
                const statusCode: number = response.status;

                if (statusCode < 300) {
                    return response.json();
                }

                return null;
            })
            .then((result: { available: number, sent: number } | null) => {
                let status: TokenStatusState;

                if (result !== null) {
                    status = {...result, ...{hasData: true}};
                    this.setState({status});
                }
                else {
                    status = {...this.state.status};
                    status.hasData = true;
                    this.setState({status});
                }
            })
            .catch(() => {
                const status: TokenStatusState = {...this.state.status};
                status.hasData = true;
                this.setState({status});
            });
    }

}

export default Token;
