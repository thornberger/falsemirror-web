import {ResultStatus} from "../elements/common/ResultStatus";
import {TokenStatusState} from "./TokenStatusState";

export interface TokenState {
    result: ResultStatus | null;
    status: TokenStatusState;
}
