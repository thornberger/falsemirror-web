export interface TokenStatusState {
    available: number;
    sent: number;
    hasData: boolean;
}
