import * as React from "react";
import "./TokenDisclaimer.css";

class TokenDisclaimer extends React.Component {
    public render(){
        return (
            <div className="TokenDisclaimer">
                <ol>
                    <li>
                        You agree that we use your e-mail address for sending an e-mail with the token.
                    </li>
                    <li>
                        Your e-mail address will be stored in an anonymized way in the database so we can resend you the e-mail if necessary.
                    </li>
                    <li>
                        We do not use the data provided by you in any other way than explained above.
                    </li>
                    <li>
                        The above statements are valid in addition to the general legal statements of this site.
                    </li>
                </ol>
            </div>
        );
    }
}

export default TokenDisclaimer;
