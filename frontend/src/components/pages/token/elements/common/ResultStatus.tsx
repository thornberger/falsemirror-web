import {ResultStatusMessage} from "./ResultStatusMessage";

export interface ResultStatus {
    message: ResultStatusMessage;
    email: string
}
