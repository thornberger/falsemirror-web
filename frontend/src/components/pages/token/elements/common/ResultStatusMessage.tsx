export enum ResultStatusMessage {
    ok = "ok",
    error = "error",
    invalidEmail = "invalidEmail",
    unableToSendMail = "unableToSendMail",
    tokensDepleted = "tokensDepleted",
}
