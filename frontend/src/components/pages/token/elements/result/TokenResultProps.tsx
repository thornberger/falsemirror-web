import {ResultStatus} from "../common/ResultStatus";

export interface TokenResultProps {
    result: ResultStatus
}
