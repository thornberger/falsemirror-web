import * as React from "react";
import {ReactNode} from "react";
import {sprintf} from "sprintf-js";
import {ResultStatusMessage} from "../common/ResultStatusMessage";
import {TokenResultProps} from "./TokenResultProps";

class TokenResult extends React.Component<TokenResultProps> {

    private static readonly MESSAGES = {
        [ResultStatusMessage.ok]: 'Your download token will be sent to %s.',
        [ResultStatusMessage.error]: 'An unexpected error occured while trying to process your request. Please try again. If that error persists please contact us.',
        [ResultStatusMessage.invalidEmail]: 'Your e-mail address %s could not be validated.',
        [ResultStatusMessage.tokensDepleted]: 'Unfortunately there are no tokens available. Come back again later or check facebook for new tokens.',
        [ResultStatusMessage.unableToSendMail]: 'There was an error sending the e-mail to %s. Please try again. If that error persists please contact us.',
    };

    public render(): ReactNode {
        return (
            <div>{this.mapMessage(this.props.result.message)}</div>
        );
    }

    private mapMessage(statusMessage: ResultStatusMessage): string {
        const message = TokenResult.MESSAGES[statusMessage] || null;
        if (message === null) {
            return "An unexpected error has occurred";
        }

        return sprintf(TokenResult.MESSAGES[statusMessage], this.props.result.email);
    }
}

export default TokenResult;
