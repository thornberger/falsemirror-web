import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {ResultStatusMessage} from "../common/ResultStatusMessage";
import TokenResult from "./TokenResult";

it('renders', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TokenResult
      result={{message: ResultStatusMessage.ok, email: "foo@bar.com"}}
  />, div);
  ReactDOM.unmountComponentAtNode(div);
});
