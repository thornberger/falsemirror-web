import {ChangeEvent, ReactNode} from "react";
import * as React from "react";
import * as isEmail from "validator/lib/isEmail";
import Button from "../../../../elements/form/button/Button";
import Captcha from "../../../../elements/form/captcha/Captcha";
import CheckBox from "../../../../elements/form/checkbox/CheckBox";
import TextField from "../../../../elements/form/textfield/TextField";
import {ResultStatusMessage} from "../common/ResultStatusMessage";
import "./TokenForm.css";
import {TokenFormProps} from "./TokenFormProps";
import {TokenFormState} from "./TokenFormState";

class TokenForm extends React.Component<TokenFormProps, TokenFormState> {

    constructor(props: TokenFormProps) {
        super(props);
        this.state = {
            acceptConditions: {
                isValid: false,
                name: "acceptedConditions",
                value: false
            },
            captchaResponse: null,
            email: {
                isValid: false,
                name: "email",
                value: ""
            },
            isValid: false
        };

        this.handleCaptchaSolved = this.handleCaptchaSolved.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleAcceptConditions = this.handleAcceptConditions.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    public render(): ReactNode {
        return (
            <div className="TokenForm">
                <TextField label="E-Mail" value={this.state.email.value} onChange={this.handleEmail}/>
                <CheckBox label="I accept the terms and conditions below" value={this.state.acceptConditions.value}
                          onChange={this.handleAcceptConditions}/>
                <Captcha handleCaptchaSolved={this.handleCaptchaSolved}/>
                <Button label="Send" isDisabled={!this.state.isValid} onClick={this.handleSubmit}/>
            </div>
        );
    }

    private handleEmail(event: ChangeEvent<HTMLInputElement>): void {
        const email = {...this.state.email};
        email.value = event.target && event.target.value || "";
        email.isValid = isEmail(email.value);

        const isValid = this.state.captchaResponse !== null && email.isValid && this.state.acceptConditions.isValid;

        this.setState({email, isValid});
    }

    private handleAcceptConditions(event: ChangeEvent<HTMLInputElement>): void {
        const acceptConditions = {...this.state.acceptConditions};
        acceptConditions.value = event.target && event.target.checked || false;
        acceptConditions.isValid = acceptConditions.value;

        const isValid = this.state.captchaResponse !== null && this.state.email.isValid && acceptConditions.isValid;

        this.setState({acceptConditions, isValid});
    }

    private handleCaptchaSolved(captchaResponse: string | null): void {
        const isValid = captchaResponse !== null && this.state.email.isValid && this.state.acceptConditions.isValid;

        this.setState({captchaResponse, isValid});
    }

    private handleSubmit(): void {
        const body = {
            captchaResponse: this.state.captchaResponse,
            email: this.state.email.value
        };

        fetch('/api/token/request', {
            body: JSON.stringify(body),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
        })
            .then((response: Response) => {
                if (response.status < 300) {
                    return response.json();
                }
                return null;
            })
            .then((data: any | null) => {
                let message = ResultStatusMessage.error;
                if (data !== null && data.hasOwnProperty("status")) {
                    message = data.status;
                }

                this.props.onResult({
                    email: this.state.email.value,
                    message
                });
            })
            .catch((error: Error) => {
                console.error(error);
            });
    }
}

export default TokenForm;
