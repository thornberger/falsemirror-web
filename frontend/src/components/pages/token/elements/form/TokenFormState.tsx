import {InputState} from "../../../../../common/input/InputState";

export interface TokenFormState {
    acceptConditions: InputState & { value: boolean };
    email: InputState  & { value: string } ;
    captchaResponse: string | null;
    isValid: boolean;
}
