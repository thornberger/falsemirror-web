import {ResultStatus} from "../common/ResultStatus";

export interface TokenFormProps {
    onResult: (result: ResultStatus) => void ;
}
