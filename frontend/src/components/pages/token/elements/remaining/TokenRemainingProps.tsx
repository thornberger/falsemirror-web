export interface TokenRemainingProps {
    total: number;
    available: number;
}
