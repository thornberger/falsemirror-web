import * as React from "react";
import {ReactNode} from "react";
import Progress from "../../../../elements/progress/Progress";
import "./TokenRemaining.css";
import {TokenRemainingProps} from "./TokenRemainingProps";

class TokenRemaining extends React.Component<TokenRemainingProps> {

    public render(): ReactNode {
        const total = this.props.total;
        const percentage = total > 0 ? this.props.available / total : 0;
        const text = `${this.props.available}/${total} codes left`;

        return (
            <div className="TokenRemaining">
                <Progress
                    value={percentage}
                    colorFill="var(--color-text-shadow)"
                    colorEmpty="var(--color-background)"
                />
                <div>{text}</div>
            </div>
        );
    }

}

export default TokenRemaining;
