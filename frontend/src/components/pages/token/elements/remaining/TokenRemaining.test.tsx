import * as React from 'react';
import * as ReactDOM from 'react-dom';
import TokenRemaining from "./TokenRemaining";

it('renders', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TokenRemaining total={2} available={1}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
