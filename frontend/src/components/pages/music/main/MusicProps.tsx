import {ReleaseData} from "../elements/common/ReleaseData";

export interface MusicProps {
    data: ReleaseData[];
}
