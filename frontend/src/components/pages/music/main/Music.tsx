import * as React from "react";
import {ReactNode} from "react";
import {ReleaseData} from "../elements/common/ReleaseData";
import Release from "../elements/release/Release";
import './Music.css';
import {MusicProps} from "./MusicProps";

class Music extends React.Component<MusicProps> {

    public constructor(props: MusicProps) {
        super(props);

        this.getAlbums = this.getAlbums.bind(this);
    }

    public componentDidMount(): void {
        document.title = "False Mirror - Music";
    }

    public render(): ReactNode {
        const albums = this.getAlbums();

        return (
            <div className="Music">
                <div className="Albums">
                    {albums}
                </div>
            </div>
        );
    }

    private getAlbums(): ReactNode[] {
        const nodes: ReactNode[] = [];

        this.props.data.forEach((releaseData: ReleaseData) => {
            nodes.push(
                <Release key={releaseData.id} data={releaseData}/>
            )
        });

        return nodes;
    }
}

export default Music;
