import * as React from "react";
import {ReactNode} from "react";
import ReleaseDetail from "../detail/ReleaseDetail";
import ReleaseThumbnail from "../thumbnail/ReleaseThumbnail";
import './Release.css';
import {ReleaseProps} from "./ReleaseProps";
import {ReleaseState} from "./ReleaseState";

class Release extends React.Component<ReleaseProps, ReleaseState> {

    constructor(props: ReleaseProps) {
        super(props);
        this.state = {
            isOpen: false,
            showPreviewName: false,
        };

        this.onOpenModal = this.onOpenModal.bind(this);
        this.onCloseModal = this.onCloseModal.bind(this);
    }

    public render(): ReactNode {
        return (
            <div className="Release">
                <div onClick={this.onOpenModal}>
                    <ReleaseThumbnail
                        name={this.props.data.name}
                        image={this.props.data.thumbnail}
                    />
                    <div className="ReleaseOverlayContainer">
                        <div>{this.props.data.name}</div>
                    </div>
                </div>
                <ReleaseDetail data={this.props.data} isOpen={this.state.isOpen} onClose={this.onCloseModal}/>
            </div>
        );
    }

    private onOpenModal(): void {
        this.setState({isOpen: true});
    };

    private onCloseModal(): void {
        this.setState({isOpen: false});
    };
}

export default Release;
