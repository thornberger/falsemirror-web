export interface ReleaseState {
    isOpen: boolean;
    showPreviewName: boolean;
}
