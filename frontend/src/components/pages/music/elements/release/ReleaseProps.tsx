import {ReleaseData} from "../common/ReleaseData";

export interface ReleaseProps {
    data: ReleaseData;
}
