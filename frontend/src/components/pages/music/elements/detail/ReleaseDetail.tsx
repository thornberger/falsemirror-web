import * as React from "react";
import {ReactNode} from "react";
import Modal from 'react-responsive-modal';
import Button from "../../../../elements/form/button/Button";
import CloseButton from "../../../../elements/modal/closebutton/CloseButton";
import ReleaseCover from "../cover/ReleaseCover";
import './ReleaseDetail.css';
import {ReleaseDetailProps} from "./ReleaseDetailProps";

class ReleaseDetail extends React.Component<ReleaseDetailProps> {

    constructor(props: ReleaseDetailProps) {
        super(props);

        this.renderBuyInformation = this.renderBuyInformation.bind(this);
        this.onBuy = this.onBuy.bind(this);
    }

    public render(): ReactNode {
        return (
            <Modal
                open={this.props.isOpen}
                onClose={this.props.onClose}
                showCloseIcon={false}
                classNames={{
                    modal: "ReleaseDetail"
                }}
            >
                <h2>{this.props.data.name}</h2>
                <CloseButton onClick={this.props.onClose}/>
                <div className="ReleaseDetailContainer">
                    <ReleaseCover name={this.props.data.name} image={this.props.data.fullCover}/>
                    <div className="ReleaseDetailData">
                        <ul>
                            <li>
                                <div className="ReleaseDetailDataKey">Released</div>
                                <div className="ReleaseDetailDataValue">{this.props.data.date}</div>
                            </li>
                            <li>
                                <div className="ReleaseDetailDataKey">Label</div>
                                <div className="ReleaseDetailDataValue">{this.props.data.label}</div>
                            </li>
                            <li>
                                <div className="ReleaseDetailDataKey">Tracklist</div>
                                <ul className="ReleaseDetailDataTracklist">
                                    {this.props.data.tracks.map((track, i) => {
                                        return (
                                            <li key={i}>{track}</li>
                                        );
                                    })}
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                {this.renderBuyInformation()}
            </Modal>
        );
    }

    private renderBuyInformation(): ReactNode {
        return (
            <div className="BuyInformation">
                <Button
                    label={this.props.data.buyLink ? "Buy" : "Sold Out"}
                    isDisabled={!this.props.data.buyLink}
                    onClick={this.onBuy}/>
            </div>
        );
    }

    private onBuy(): void {
        if (this.props.data.buyLink) {
            window.location.href = this.props.data.buyLink;
        }
    }
}

export default ReleaseDetail;
