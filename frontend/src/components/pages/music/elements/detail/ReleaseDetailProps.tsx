import {ReleaseData} from "../common/ReleaseData";

export interface ReleaseDetailProps {
    data: ReleaseData;
    isOpen: boolean;
    onClose: () => void;

}
