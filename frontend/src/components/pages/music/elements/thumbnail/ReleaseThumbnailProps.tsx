import {ImageData} from "../../../../elements/image/ImageData";

export interface ReleaseThumbnailProps {
    name: string;
    image: ImageData
}
