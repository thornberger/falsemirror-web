import * as React from "react";
import {ReactNode} from "react";
import {Image} from "../../../../elements/image/Image";
import {ImageType} from "../../../../elements/image/ImageType";
import "./ReleaseThumbnail.css";
import {ReleaseThumbnailProps} from "./ReleaseThumbnailProps";

class ReleaseThumbnail extends React.Component<ReleaseThumbnailProps> {

    public render(): ReactNode {
        return (
            <Image
                name={this.props.name}
                type={ImageType.fixed}
                data={this.props.image}
                className="ReleaseThumbnail"
            />
        );
    }
}

export default ReleaseThumbnail;
