import {ImageData} from "../../../../elements/image/ImageData";

export interface ReleaseData {
    id: string;
    name: string;
    date: string;
    label: string;
    tracks: string[];
    buyLink?: string | null;
    thumbnail: ImageData;
    fullCover: ImageData;
}
