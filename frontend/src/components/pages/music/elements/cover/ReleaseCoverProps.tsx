import {ImageData} from "../../../../elements/image/ImageData";

export interface ReleaseCoverProps {
    name: string;
    image: ImageData
}
