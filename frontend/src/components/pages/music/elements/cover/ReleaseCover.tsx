import * as React from "react";
import {ReactNode} from "react";
import {Image} from "../../../../elements/image/Image";
import {ImageType} from "../../../../elements/image/ImageType";
import "./ReleaseCover.css";
import {ReleaseCoverProps} from "./ReleaseCoverProps";

class ReleaseCover extends React.Component<ReleaseCoverProps> {

    public render(): ReactNode {
        return (
            <Image
                name={this.props.name}
                type={ImageType.fluid}
                data={this.props.image}
                className="ReleaseCover"
                imageStyle={{width: "100%"}}
            />
        );
    }
}

export default ReleaseCover;
