import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Header from "./Header";

jest.mock('react-svg', () => ({
    ReactSVG: <div>MockReactSVG</div>
}));

jest.mock('../navigation/Navigation', () => ()=> <div />);

xit('renders', () => {
    // FIXME needs fixing

    const div = document.createElement('div');
    ReactDOM.render(<Header logoPath="foo/bar.svg"/>, div);
    ReactDOM.unmountComponentAtNode(div);
});
