import * as React from "react";
import {ReactNode} from "react";
import ReactSVG from "react-svg";
import Navigation from "../navigation/Navigation";
import "./Header.css"
import {HeaderProps} from "./HeaderProps";

class Header extends React.Component<HeaderProps> {

    public render(): ReactNode {
        return (
            <div className="Header">
                <h1>False Mirror</h1>
                <ReactSVG src={this.props.logoPath} className="Logo"/>
                <Navigation/>
            </div>
        );
    }
}

export default Header;
