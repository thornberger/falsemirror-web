import * as React from "react";
import {ReactNode} from "react";
import './Main.css';

class Main extends React.Component {

    public render(): ReactNode {
        return (
            <div className="Main">
                {this.props.children}
            </div>
        );
    }
}

export default Main;
