import GatsbyLink from "gatsby-link";
import * as React from "react";
import {ReactNode} from "react";
import "./Fooder.css";

class Fooder extends React.Component {

    public render(): ReactNode {
        return (
            <div className="Fooder">
                ©2018 Tobias Hornberger - All Rights Reserved
                <nav>
                    <GatsbyLink to="/legal">Legal Statement</GatsbyLink>
                </nav>
            </div>
        );
    }
}

export default Fooder;
