import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Fooder from "./Fooder";

beforeEach(()=> {
  // @ts-ignore
  global.___loader = {
    enqueue: jest.fn()
  };
});

it('renders', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Fooder />, div);
  ReactDOM.unmountComponentAtNode(div);
});
