import {faBandcamp, faFacebook, faSoundcloud, faYoutube} from "@fortawesome/free-brands-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import * as React from "react";
import {ReactNode} from "react";
import './Social.css';

class Social extends React.Component {

    public render(): ReactNode {
        return (
            <ul className="Social">
                <li><a href="https://facebook.com/falsemirror.de"><FontAwesomeIcon icon={faFacebook} size="2x"/>Facebook</a></li>
                <li><a href="https://soundcloud.com/false_mirror"><FontAwesomeIcon icon={faSoundcloud} size="2x"/>SoundCloud</a></li>
                <li><a href="https://bandcamp.falsemirror.de"><FontAwesomeIcon icon={faBandcamp} size="2x"/>Bandcamp</a></li>
                <li><a href="https://www.youtube.com/c/FalseMirror"><FontAwesomeIcon icon={faYoutube} size="2x"/>YouTube</a></li>
            </ul>
        );
    }
}

export default Social;
