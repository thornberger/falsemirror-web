import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Social from './Social';

it('renders', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Social/>, div);
    ReactDOM.unmountComponentAtNode(div);
});
