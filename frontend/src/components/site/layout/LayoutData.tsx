export interface LayoutData {
    logo: {
        publicURL: string
    }
    site: {
        meta: {
            siteUrl: string,
            title: string,
            description: string
            googleSiteVerification: string
        }
    }

}
