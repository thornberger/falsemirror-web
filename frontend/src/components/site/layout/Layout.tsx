import * as React from 'react';
import './Layout.css';

import {graphql, StaticQuery} from "gatsby";
import {ReactNode} from "react";
import Helmet from "react-helmet";
import Fooder from "../fooder/Fooder";
import Header from "../header/Header";
import Main from "../main/Main";
import Social from "../social/Social";
import {LayoutData} from "./LayoutData";
import {LayoutProps} from "./LayoutProps";

class Layout extends React.Component<LayoutProps> {

    public constructor(props: LayoutProps) {
        super(props);

        this.renderWithData = this.renderWithData.bind(this);
    }

    public render(): ReactNode {
        return (
            <StaticQuery
                query={
                    graphql`
                        query LogoQuery {
                            logo: file(relativePath: {eq: "images/logo.svg"}) {
                                publicURL
                            }
                            site {
                                meta: siteMetadata {
                                  siteUrl
                                  title
                                  description
                                  googleSiteVerification
                                }
                            }
                        }`
                }
                render={this.renderWithData}
            />
        );
    }

    public renderWithData(data: LayoutData) {
        let canonical = data.site.meta.siteUrl;
        if (this.props.path.length > 0) {
            canonical += '/' + this.props.path;
        }

        return (
            <div className="Layout">
                <Helmet>
                    <title>{data.site.meta.title} | {this.props.subtitle}</title>
                    <link rel="canonical" href={canonical}/>
                    <meta name="description" content={data.site.meta.description}/>
                    <meta name="google-site-verification" content={data.site.meta.googleSiteVerification}/>
                    <html lang="en"/>
                </Helmet>
                <Header logoPath={data.logo.publicURL}/>
                <Main>
                    {this.props.children}
                </Main>
                <Social/>
                <Fooder/>
            </div>
        );
    }
}

export default Layout;
