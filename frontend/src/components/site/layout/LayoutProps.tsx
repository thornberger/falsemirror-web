import {ReactNode} from "react";

export interface LayoutProps {
    path: string
    subtitle: string
    children: ReactNode;
}
