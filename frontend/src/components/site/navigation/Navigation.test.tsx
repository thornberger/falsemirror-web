import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Navigation from "./Navigation";

beforeEach(()=> {
  // @ts-ignore
  global.___loader = {
    enqueue: jest.fn()
  };
});

it('renders', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Navigation />, div);
  ReactDOM.unmountComponentAtNode(div);
});
