import {faBars, faTimes} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Link} from "gatsby";
import * as React from "react";
import {ReactNode} from "react";
import "./Navigation.css";
import {NavigationState} from "./NavigationState";

class Navigation extends React.Component<any, NavigationState> {

    constructor(props: any) {
        super(props);
        this.state = {
            isCollapsed: true
        };

        this.toggleMenu = this.toggleMenu.bind(this);
        this.hideMenu = this.hideMenu.bind(this);
    }

    public render(): ReactNode {
        return (
            <div className="Navigation">
                <div className="Links" style={{display: this.state.isCollapsed ? "none" : "flex" }}>
                    <nav>
                        <Link className="NavigationLink" activeClassName="Active" to="/" onClick={this.hideMenu}>Home</Link>
                    </nav>
                    <nav>
                        <Link className="NavigationLink" activeClassName="Active" to="/music" onClick={this.hideMenu}>Music</Link>
                    </nav>
                    <nav>
                        <Link className="NavigationLink" activeClassName="Active" to="/live" onClick={this.hideMenu}>Live</Link>
                    </nav>
                    <nav>
                        <Link className="NavigationLink" activeClassName="Active" to="/about" onClick={this.hideMenu}>About</Link>
                    </nav>
                </div>
                <div className="Toggle" onClick={this.toggleMenu}>
                    <FontAwesomeIcon icon={this.state.isCollapsed ? faBars : faTimes}/>
                </div>
            </div>
        );
    }

    private toggleMenu(): void {
        this.setState((state: NavigationState) => ({
            isCollapsed: !state.isCollapsed
        }));
    }

    private hideMenu(): void {
        this.setState(() => ({
            isCollapsed: true
        }));
    }
}

export default Navigation;
