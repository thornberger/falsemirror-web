import * as React from "react";
import {ReactNode} from "react";
import Layout from "../components/site/layout/Layout";

export default class NotFound extends React.Component {
    public render(): ReactNode {
        return (
            <Layout path="404" subtitle="Page Not Found">
                <h2>404 Not Found</h2>
                <p>The page you are looking for doesn't exist.</p>
            </Layout>
        );
    }
}
