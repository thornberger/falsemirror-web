import * as React from "react";
import {ReactNode} from "react";
import Token from "../components/pages/token/main/Token";
import Layout from "../components/site/layout/Layout";

export default class Index extends React.Component {
    public render(): ReactNode {
        return (
            <Layout path="sg-token" subtitle="SIGINT Bonus Download">
                <Token/>
            </Layout>
        );
    }
}
