import {graphql} from "gatsby"
import * as React from "react";
import {ReactNode} from "react";
import {ReleaseData} from "../components/pages/music/elements/common/ReleaseData";
import Music from "../components/pages/music/main/Music";
import Layout from "../components/site/layout/Layout";

interface MusicProps {
    data: {
        releases: {
            edges: [{
                release: ReleaseNode
            }]
        }
    }
}

interface ReleaseNode {
    identifier: string,
    name: string,
    releaseDate: string,
    label: string,
    tracks: [{
        title: string
    }]
    buyLink: string,
    cover: {
        thumbnail: ImageData
        full: ImageData
    }
}

// noinspection JSUnusedGlobalSymbols
export default class Index extends React.Component<MusicProps> {

    public constructor(props: MusicProps) {
        super(props);

        this.getReleaseData = this.getReleaseData.bind(this);
    }


    public render(): ReactNode {
        const releaseData = this.getReleaseData();

        return (
            <Layout path="music" subtitle="Music">
                <Music data={releaseData}/>
            </Layout>
        );
    }

    private getReleaseData(): ReleaseData[] {
        return this.props.data.releases.edges.map((node: { release: ReleaseNode }) => {
            const release: ReleaseNode = node.release;
            const tracks: string[] = [];

            release.tracks.forEach((track) => {
                if(track && track.title){
                    tracks.push(track.title);
                }
            });

            const id = node.release.identifier;

            return {
                buyLink: node.release.buyLink,
                date: node.release.releaseDate || "",
                fullCover: node.release.cover.full,
                id,
                label: node.release.label || "",
                name: node.release.name || "",
                thumbnail: node.release.cover.thumbnail,
                tracks
            };
        });
    }
}

export const query = graphql`
query MusicQuery {
  releases: allContentfulRelease(sort: {fields:releaseDate, order: DESC}) {
    edges {
      release: node {
        identifier
        name
        releaseDate
        label
        tracks {
          title
        }
        cover {
          thumbnail: fixed(width:200, quality:100){
            ...GatsbyContentfulFixed_withWebp
          }
          full: fluid(maxWidth:1600,quality: 100){
            ...GatsbyContentfulFluid_withWebp
          }
        }
        buyLink
      }
    }
  }
}
`;
