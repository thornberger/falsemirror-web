import {graphql} from "gatsby";
import * as React from "react";
import {ReactNode} from "react";
import Content from "../components/elements/content/Content";
import Layout from "../components/site/layout/Layout";

interface PageTemplateProps {
    data: {
        page: {
            slug: string,
            title: string,
            content: {
                data: {
                    html: string;
                }
            },
            titleImage: {
                image: ImageData
            }
        }
    }
}

// noinspection JSUnusedGlobalSymbols
export default class PageTemplate extends React.Component<PageTemplateProps> {

    private static readonly NO_CONTENT_TEXT = '<p>No content was found for this page.</p>';

    public constructor(props: PageTemplateProps) {
        super(props);

        this.getSlug = this.getSlug.bind(this);
        this.getText = this.getText.bind(this);
        this.getTitle = this.getTitle.bind(this);
        this.getTitleImage = this.getTitleImage.bind(this);
    }

    public render(): ReactNode {
        return (
            <Layout
                path={this.getSlug()}
                subtitle={this.getTitle()}
            >
                <Content text={this.getText()}
                         titleImage={this.getTitleImage()}
                />
            </Layout>
        );
    }

    private getSlug(): string {
        let slug = this.props.data.page.slug;
        if (slug === 'index') {
            slug = '';
        }
        return slug;
    }

    private getText(): string {
        if (this.props.data && this.props.data.page &&
            this.props.data.page.content &&
            this.props.data.page.content.data &&
            this.props.data.page.content.data.html) {
            return this.props.data.page.content.data.html;
        }

        return PageTemplate.NO_CONTENT_TEXT;
    }

    private getTitle(): string {
        if (this.props.data && this.props.data.page &&
            this.props.data.page.title) {
            return this.props.data.page.title;
        }

        throw new Error(`No title found for page ${this.props.data.page.slug}.`)
    }

    private getTitleImage(): ImageData | undefined {
        if (this.props.data && this.props.data.page &&
            this.props.data.page.titleImage &&
            this.props.data.page.titleImage.image) {
            return this.props.data.page.titleImage.image;
        }

        return;
    }
}

export const query = graphql`
    query PageTemplateQuery($name: String!) {
        page: contentfulPage(slug: {eq: $name}) {
            slug
            title
            content {
                data: childMarkdownRemark {
                    html
                }
            }
            titleImage {
                image: fluid(maxWidth:200,quality: 100){
                    ...GatsbyContentfulFluid_withWebp
                }
            }
        }
    }
`;
