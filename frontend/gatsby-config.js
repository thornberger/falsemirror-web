require('dotenv').config();

module.exports = {
    siteMetadata: {
        siteUrl: 'https://www.falsemirror.de',
        title: 'False Mirror | Deep Organic Dark Ambient',
        description: 'The official website of False Mirror, Deep Organic Dark Ambient by Tobias Hornberger.',
        googleSiteVerification: process.env.GOOGLE_SITE_VERIFICATION || '',
    },
    pathPrefix: '/staging',
    proxy: {
        prefix: '/api',
        url: 'http://localhost:5000',
    },
    plugins: [
        'gatsby-plugin-react-helmet',
        {
            resolve: 'gatsby-plugin-typescript',
            options: {
                transpileOnly: true,
                compilerOptions: {
                    target: 'esnext',
                    experimentalDecorators: true,
                    jsx: 'react',
                },
            },
        },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'data',
                path: `${__dirname}/data/`,
            },
        },
        {
            resolve: 'gatsby-transformer-json',
            options: {
                typeName: ({ node, object, isArray }) => {
                    const relativeDirectory = node.relativeDirectory;
                    const parts = relativeDirectory.split('/').map((part) => {
                        if(part.length === 1){
                            return part.toUpperCase();
                        }

                        return part.charAt(0).toUpperCase() + part.substr(1);
                    });

                    return parts.join('');
                },
            },
        },
        {
            resolve: 'gatsby-transformer-remark',
            options: {
                plugins: [
                    {
                        resolve: 'gatsby-remark-images',
                        options: {
                            maxWidth: 200,
                            linkImagesToOriginal: false,
                            withWebp: true,
                            wrapperStyle: `float: left; border-radius: 3%; width: 30%; height: 30%; margin: 5px 20px 0 5px;`,
                        },
                    },
                ],
            },
        },
        {
            resolve: `gatsby-source-contentful`,
            options: {
                spaceId: process.env.CONTENTFUL_SPACE_ID || '',
                accessToken: process.env.CONTENTFUL_ACCESS_TOKEN || '',
            },
        },
        {
            resolve: 'gatsby-plugin-manifest',
            options: {
                name: 'false-mirror',
                short_name: 'fm-web',
                start_url: '/',
                background_color: '#f1f1f1',
                theme_color: '#f1f1f1',
                display: 'minimal-ui',
                icon: './data/images/icon.png'
            },
        },
        {
            resolve: `gatsby-plugin-google-analytics`,
            options: {
                trackingId: process.env.GOOGLE_ANALYTICS_TRACKING_ID || '',
                head: false,
                anonymize: true,
                respectDNT: true
            },
        },
        {
            resolve: 'gatsby-plugin-robots-txt',
            options: {
                host: 'https://www.falsemirror.de',
                sitemap: 'https://www.falsemirror.de/sitemap.xml',
                policy: [{ userAgent: '*', disallow: '/sg-token' }]
            }
        },
        {
            resolve: 'gatsby-plugin-web-font-loader',
            options: {
                google: {
                    families: ['Open Sans:400,600']
                },
                timeout: 3000
            }
        },
        'gatsby-plugin-sitemap',
        'gatsby-transformer-sharp',
        'gatsby-plugin-sharp',
        'gatsby-plugin-offline',
        'gatsby-plugin-remove-trailing-slashes'
    ],
};
